timestamps {
    node('built-in'){
        def branchName = params.BRANCH.replace("origin/", "")
        println "BRANCH=$BRANCH"
        println "URL=${env.GIT_HOST}"
        println "branchName=$branchName"
        stage('Checkout') {
            git branch: branchName, credentials: 'dhgouveia2-gitlab', url: "${env.GIT_HOST}/test01.git"
        }
    }
}
